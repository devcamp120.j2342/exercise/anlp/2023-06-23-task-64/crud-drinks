package com.devcamp.apidrinks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.apidrinks.model.CDrink;

public interface CDrinkRepository extends JpaRepository<CDrink, Long>{
    
}
