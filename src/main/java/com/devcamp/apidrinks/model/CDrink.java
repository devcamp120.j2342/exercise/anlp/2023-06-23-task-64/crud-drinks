package com.devcamp.apidrinks.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="drink")
public class CDrink {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="drink_id")
    private long id;

    @Size(min = 2, message = "tên nước uống có ít nhất 2 ký tự")
    @Column(name= "ten_nuoc_uong")
    private String tenNuocUong;

    @NotNull(message = "nhập mã nước uống")
    @Size(min = 2, message = "mã nước uống có ít nhất 2 ký tự")
    @Column(name= "ma_nuoc_uong")
    private String maNuocUong;

    @NotNull(message = "nhập đơn giá")
    @Range(min = 10000, max = 50000, message = "giá trị từ 10000 đến 50000")
    @Column(name= "don_gia")
    private long donGia;

    @Column(name= "ghi_chu")
    private String ghiChu;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name= "ngay_tao", nullable = true, updatable = false)
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name= "ngay_cap_nhat", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;

    public CDrink() {
    }
    public CDrink(long id, String tenNuocUong, String maNuocUong, long donGia, String ghiChu, Date ngayTao,
            Date ngayCapNhat) {
        this.id = id;
        this.tenNuocUong = tenNuocUong;
        this.maNuocUong = maNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public long getDonGia() {
        return donGia;
    }
    public void setDonGia(long donGia) {
        this.donGia = donGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
