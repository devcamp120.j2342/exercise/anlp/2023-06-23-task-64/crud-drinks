package com.devcamp.apidrinks.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.apidrinks.model.CDrink;
import com.devcamp.apidrinks.repository.CDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    
    @Autowired
    private CDrinkRepository drinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> drinks = new ArrayList<>();
            drinkRepository.findAll().forEach(drinks::add);
            return new ResponseEntity<>(drinks, HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable(value = "id") long id) {
        try {
            Optional<CDrink> drinkOptional = drinkRepository.findById(id);
            if(drinkOptional.isPresent()){
                CDrink drink = drinkOptional.get();
                return new ResponseEntity<>(drink, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/created-drink")
    public ResponseEntity<CDrink> createdDrink(@Valid @RequestBody CDrink drink) {
        try {
            drink.setNgayTao(new Date());
            drink.setNgayCapNhat(null);
            CDrink createdDrink = drinkRepository.save(drink);
            return new ResponseEntity<>(createdDrink, HttpStatus.CREATED);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update-drink/{id}")
    public ResponseEntity<CDrink> updateDrink(@Valid @PathVariable("id") long id, @RequestBody CDrink uDrink){
        try {
            Optional<CDrink> existingDrink = drinkRepository.findById(id);
            if (existingDrink.isPresent()) {
                CDrink drink = existingDrink.get();
                drink.setMaNuocUong(uDrink.getMaNuocUong());
                drink.setTenNuocUong(uDrink.getTenNuocUong());
                drink.setDonGia(uDrink.getDonGia());
                drink.setGhiChu(uDrink.getGhiChu());
                drink.setNgayTao(new Date());
                drink.setNgayCapNhat(null);
                CDrink updatedDrinkEntity = drinkRepository.save(drink);
                return new ResponseEntity<>(updatedDrinkEntity, HttpStatus.OK);
            } else {
                 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete-drink/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id) {
        try {
            drinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e) {
             return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete-drink")
    public ResponseEntity<CDrink> deleteAllDrink() {
        try {
            drinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e) {
             return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
